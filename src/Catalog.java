import java.util.HashMap;
import java.util.Map;

public class Catalog<Thing> {
    private Map<Thing, LibraryItem<Thing>> items = new HashMap<>();

    public void addItem(LibraryItem<Thing> item) {
        items.put(item.getItemID(), item);
    }

    public void removeItem(Thing itemID) throws Exception {
        if (!items.containsKey(itemID)) {
            throw new Exception("Item not found");
        }
        items.remove(itemID);
    }

    public LibraryItem<Thing> getItem(Thing itemID) throws Exception {
        if (!items.containsKey(itemID)) {
            throw new Exception("Item not found");
        }
        return items.get(itemID);
    }

    public void displayCatalog() {
        for (LibraryItem<Thing> item : items.values()) {
            System.out.println("Item ID: " + item.getItemID() + ", Title: " + item.getTitle() + ", Author: " + item.getAuthor());
        }
    }
}
