public class LibraryItem<Thing> {
    private String title;
    private String author;
    private Thing itemID;

    public LibraryItem(String title, String author, Thing itemID) {
        this.title = title;
        this.author = author;
        this.itemID = itemID;
    }

    // Getters and setters
    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Thing getItemID() {
        return itemID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setItemID(Thing itemID) {
        this.itemID = itemID;
    }
}