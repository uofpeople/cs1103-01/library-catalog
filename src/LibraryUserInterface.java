import java.util.Scanner;

public class LibraryUserInterface {
    public static void main(String[] args) {
        Catalog<Integer> catalog = new Catalog<>();
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;

        while (!exit) {
            System.out.println("Choose an option: \n1. Add Item \n2. Remove Item \n3. Display Catalog \n4. Exit");
            int choice = scanner.nextInt();
            scanner.nextLine(); // consume newline

            switch (choice) {
                case 1:
                    System.out.println("Enter title:");
                    String title = scanner.nextLine();
                    System.out.println("Enter author:");
                    String author = scanner.nextLine();
                    System.out.println("Enter item ID:");
                    int itemID = scanner.nextInt();
                    catalog.addItem(new LibraryItem<>(title, author, itemID));
                    break;
                case 2:
                    System.out.println("Enter item ID to remove:");
                    int removeID = scanner.nextInt();
                    try {
                        catalog.removeItem(removeID);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    catalog.displayCatalog();
                    break;
                case 4:
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
            }
        }
        scanner.close();
    }
}